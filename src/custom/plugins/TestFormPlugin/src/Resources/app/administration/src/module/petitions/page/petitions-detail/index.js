import template from './petitions-detail.html.twig';

const { Component, Mixin } = Shopware;
const { Criteria } = Shopware.Data;

Component.register('petitions-detail', {
    template,

    inject: ['repositoryFactory', 'systemConfigApiService'],

    mixins: [
        Mixin.getByName('placeholder'),
        Mixin.getByName('notification'),
        Mixin.getByName('discard-detail-page-changes')('petition')
    ],

    shortcuts: {
        'SYSTEMKEY+S': 'onSave',
        ESCAPE: 'onCancel'
    },

    props: {
        petitionId: {
            type: String,
            required: false,
            default: null
        }
    },


    data() {
        return {
            petition: null,
            isLoading: false,
            isSaveSuccessful: false,
            reasons: [],
        };
    },

    metaInfo() {
        return {
            title: this.$createTitle(this.identifier)
        };
    },

    computed: {
        identifier() {
            return this.placeholder(this.petition, 'name');
        },

        petitionRepository() {
            return this.repositoryFactory.create('test_form_plugin_petitions');
        },

        petitionCriteria() {
            return new Criteria();
        }
    },


    created() {
        this.systemConfigApiService.getValues('TestFormPlugin.config').then((response) => {
            this.reasons = response['TestFormPlugin.config.reasons'];
        });
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.isLoading = true;
            this.petitionRepository.get(
                this.petitionId,
                Shopware.Context.api,
                this.petitionCriteria
            ).then((petition) => {
                this.petition = petition
                this.isLoading = false;
            });
        },

        onSave() {
            this.isLoading = true;
            this.petition.hasChanges = false;

            return this.petitionRepository.save(this.petition, Shopware.Context.api).then(() => {
                this.isLoading = false;
                this.isSaveSuccessful = true;
                this.saveSuccessNotification();
                if (this.petitionId === null) {
                    this.$router.push({ name: 'testform.petitions.detail', params: { id: this.petition.id } });
                    return;
                }

                this.createdComponent();

            }).catch((exception) => {
                this.isLoading = false;
                this.createNotificationError({
                    title: this.$tc('global.default.error'),
                    message: this.$tc(
                        'global.notification.notificationSaveErrorMessage', 0, { entityName: 'Petition' }
                    )
                });
                throw exception;
            });
        },

        onLeaveProductAdd() {
            this.createdComponent();
        },

        onCancel() {
            this.$router.push({ name: 'testform.petitions.list' });
        },


        saveSuccessNotification() {
            const notificationSuccess = {
                message: 'Saved'
            };
            this.createNotificationSuccess(notificationSuccess);
        }
    }
});
