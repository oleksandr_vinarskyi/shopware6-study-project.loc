import './page/petitions-list';
import './page/petitions-detail';
import deDE from './snippet/de-DE';
import enGB from './snippet/en-GB';

Shopware.Module.register('testform-petitions', {
    type: 'plugin',
    name: 'Petitions',
    title: 'testform-petitions.general.mainMenuItemGeneral',
    description: 'testform-petitions.general.descriptionTextModule',
    color: '#ff3d58',
    icon: 'default-shopping-paper-bag-product',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        list: {
            component: 'petitions-list',
            path: 'list'
        },
        detail: {
            component: 'petitions-detail',
            path: 'detail/:id',
            props: {
                default(route) {
                    return {
                        petitionId: route.params.id
                    };
                }
            }
        }
    },

    navigation: [{
        label: 'testform-petitions.general.mainMenuItemGeneral',
        color: '#ff3d58',
        path: 'testform.petitions.list',
        icon: 'default-shopping-paper-bag-product',
        position: 100,
        parent: 'sw-customer',
    }]
});