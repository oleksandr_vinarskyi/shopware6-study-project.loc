import template from './petitions-list.html.twig'
const { Criteria } = Shopware.Data;
const { Component, Mixin } = Shopware;

Component.register('petitions-list', {
    template,

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    inject: ['repositoryFactory'],

    mixins: [
        Mixin.getByName('listing')
    ],

    data() {
        return {
            petitions: null,
            isLoading: true,
            total: 0,
        };
    },

    computed: {
        repository() {
            return this.repositoryFactory.create('test_form_plugin_petitions');
        },

        columns() {
            return [ {
                property: 'createdAt',
                label: this.$tc('testform-petitions.list.date'),
                allowResize: true,
            },{
                property: 'name',
                label: this.$tc('testform-petitions.list.name'),
                allowResize: true,
            }, {
                property: 'email',
                label: this.$tc('testform-petitions.list.email'),
                allowResize: true,
            }, {
                property: 'phone',
                label: this.$tc('testform-petitions.list.phone'),
                allowResize: true,
            }, {
                property: 'city',
                label: this.$tc('testform-petitions.list.city'),
                allowResize: true,
            }, {
                property: 'reason',
                label: this.$tc('testform-petitions.list.reason'),
                allowResize: true,
            }, {
                property: 'message',
                label: this.$tc('testform-petitions.list.message'),
                allowResize: true,
            }];
        },

        criteria() {
            const criteria = new Criteria();

            criteria.addSorting(Criteria.sort('createdAt', 'DESC', true));

            return criteria;
        }
    },

    methods: {
        getList() {
            this.isLoading = true;

            return this.repository.search(this.criteria, Shopware.Context.api)
                .then((searchResult) => {

                    this.petitions = searchResult;
                    this.total = searchResult.total;
                    this.isLoading = false;
                });
        }
    },

});