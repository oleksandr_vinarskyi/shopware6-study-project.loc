<?php declare(strict_types=1);

namespace Test\Plugin\TestFormPlugin\Core\Entity\Petition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

class PetitionCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return PetitionEntity::class;
    }
}