<?php declare(strict_types=1);


namespace Test\Plugin\TestFormPlugin\Core\Entity\Petition;


use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;

class PetitionDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'test_form_plugin_petitions';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }
    public function getEntityClass(): string
    {
        return PetitionEntity::class;
    }
    public function getCollectionClass(): string
    {
        return PetitionCollection::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
            (new StringField('name', 'name')),
            (new StringField('email', 'email')),
            (new StringField('phone', 'phone')),
            (new StringField('city', 'city')),
            (new StringField('reason', 'reason')),
            (new StringField('message', 'message')),
        ]);
    }
}