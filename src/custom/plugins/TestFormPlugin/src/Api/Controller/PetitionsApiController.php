<?php declare(strict_types=1);

namespace Test\Plugin\TestFormPlugin\Api\Controller;

use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Shopware\Core\Framework\Context;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;

/**
 * @RouteScope(scopes={"api"})
 */
class PetitionsApiController extends AbstractController
{
    /**
     * @var EntityRepositoryInterface
     */
    protected $petitionsRepository;

    public function __construct(EntityRepositoryInterface $petitionsRepository)
    {
        $this->petitionsRepository = $petitionsRepository;
    }

}