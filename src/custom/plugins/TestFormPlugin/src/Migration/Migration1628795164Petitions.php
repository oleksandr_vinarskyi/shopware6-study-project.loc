<?php declare(strict_types=1);

namespace Test\Plugin\TestFormPlugin\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1628795164Petitions extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1628795164;
    }

    public function update(Connection $connection): void
    {
        $sql = "
        CREATE TABLE IF NOT EXISTS `test_form_plugin_petitions` (
          `id` BINARY(16) NOT NULL,
          `name` VARCHAR(255) COLLATE utf8mb4_unicode_ci,
          `email` VARCHAR(255) COLLATE utf8mb4_unicode_ci,
          `phone` VARCHAR(255) COLLATE utf8mb4_unicode_ci,
          `city` VARCHAR(255) COLLATE utf8mb4_unicode_ci,
          `reason` VARCHAR(255) COLLATE utf8mb4_unicode_ci,
          `message` VARCHAR(255) COLLATE utf8mb4_unicode_ci,
          `created_at` DATETIME(3) NOT NULL,
          `updated_at` DATETIME(3) NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ";

        if (method_exists($connection, 'executeStatement')) {
            $connection->executeStatement($sql);
        } else if (method_exists($connection, 'executeQuery')) {
            $connection->executeQuery($sql);
        }

    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
