<?php declare(strict_types=1);


namespace Test\Plugin\TestFormPlugin\Storefront\Controller;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @RouteScope(scopes={"storefront"})
 */
class FormHandlerController extends StorefrontController
{
    /**
     * @var EntityRepositoryInterface
     */
    protected $petitionsRepository;

    public function __construct(EntityRepositoryInterface $petitionsRepository)
    {
        $this->petitionsRepository = $petitionsRepository;
    }

    /**
     * @Route("/send-form", name="frontend.TestFormPlugin.sendForm", methods={"POST", "GET"})
     * @param Request $request
     * @param Context $context
     * @return Response
     */
    public function sendForm(Request $request, Context $context): Response
    {
        if ($request->getMethod() === 'POST') {
            $this->petitionsRepository->create([
                $request->request->all()
            ], $context);
            return $this->redirectToRoute('frontend.TestFormPlugin.sendForm');
        } else {
            return $this->renderStorefront('@TestPluginForm/storefront/page/testform/success.html.twig');
        }
    }
}